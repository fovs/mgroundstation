%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   FOVS Open com port
%   Fibre Optic Vibration Experiment, www.fovs.de
%
%   Author: Thomas Gruebler, thomas.gruebler@tum.de
%   Date: 30.11.2013
%
%  "THE BEER-WARE LICENSE" (Revision 42):
%  <thomas@gruebler.at> wrote this file. As long as you retain this notice you
%  can do whatever you want with this stuff. If we meet some day, and you think
%  this stuff is worth it, you can buy me a beer in return
%  (preferably on the Oktoberfest) - Thomas Gr�bler
%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

s = serial('COM5');
set(s, 'BaudRate', 38400, 'Terminator', '', 'Timeout', 5);
set(s, 'InputBufferSize', 2048);
fopen(s);
s

%fclose(s)
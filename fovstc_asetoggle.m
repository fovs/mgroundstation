%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   FOVS Telecommand Toogle ASE
%   Fibre Optic Vibration Experiment, www.fovs.de
%      
%   Author: Thomas Gruebler, thomas.gruebler@tum.de
%   Date: 10.12.2013
%
%  "THE BEER-WARE LICENSE" (Revision 42):
%  <thomas@gruebler.at> wrote this file. As long as you retain this notice you
%  can do whatever you want with this stuff. If we meet some day, and you think
%  this stuff is worth it, you can buy me a beer in return 
%  (preferably on the Oktoberfest) - Thomas Gr�bler
%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sync1 = uint8(hex2dec('f9'));
sync2 = uint8(hex2dec('af'));
mid = bitor(hex2dec('20'), 4);
crch = hex2dec('76');
crcl = hex2dec('94');
cmd = [sync1 sync2 mid zeros(1, 19) crch crcl];

fwrite(s, cmd);

clear crch;
clear crcl;
clear mid;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   FOVS Telecommand Selftest
%   Fibre Optic Vibration Experiment, www.fovs.de
%      
%   Author: Thomas Gruebler, thomas.gruebler@tum.de
%   Date: 03.12.2013
%
%  "THE BEER-WARE LICENSE" (Revision 42):
%  <thomas@gruebler.at> wrote this file. As long as you retain this notice you
%  can do whatever you want with this stuff. If we meet some day, and you think
%  this stuff is worth it, you can buy me a beer in return 
%  (preferably on the Oktoberfest) - Thomas Gr�bler
%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%sync = uint8(hex2dec('aa'));
mid = uint8(bitor(hex2dec('a0'), 2));
crch = uint8(hex2dec('cb'));
crcl = uint8(hex2dec('3e'));
cmd = [sync1 sync2 mid zeros(1, 19) crch crcl];

fwrite(s, cmd);

clear crch;
clear crcl;
clear mid;
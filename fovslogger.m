%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   FOVS Live View
%   Fibre Optic Vibration Experiment, www.fovs.de
%      
%   Author: Thomas Gruebler, thomas.gruebler@tum.de
%   Date: 30.11.2013
%
%  "THE BEER-WARE LICENSE" (Revision 42):
%  <thomas@gruebler.at> wrote this file. As long as you retain this notice you
%  can do whatever you want with this stuff. If we meet some day, and you think
%  this stuff is worth it, you can buy me a beer in return 
%  (preferably on the Oktoberfest) - Thomas Gr�bler
%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Welcome message
disp('%-----------------------------------------------------%');
disp('%');
disp('% Hello!');
disp('% Welcome to the FOVS Data Logging software.');
disp('%');
disp('% Before initial run, run "fovsopen" to open the serial port.');
disp('% You may have to edit it to open the correct port number.');
disp('%');
disp('% Press "Q" (=shift+q) to quit and save all data to results/flight....mat');
disp('% Other commands:');
disp('%    "X": Quit, save and close plot window.');
disp('%    "P": Pause until "space" pressed. Attention, only 2048 bytes of data buffer!');
disp('%    "R": Reset via telecommand');
disp('%    "L": Toogle ASE ON/OFF (deactivated in firmware)');
disp('%');
disp('% After use, quit the serial port using "fovsclose".');
disp('% Other useful commands: fovstc_reset, fovstc_format, fovstc_selftest');
disp('% If serial port is tricky to open, call instrreset.');
disp('%');
disp('%-----------------------------------------------------%');
%disp('');
%disp('Press any key to continue');
%pause;

%% Init
fread(s); %flush
close all; %close open figure

% Sync bytes and message id's
sync1 = uint16(hex2dec('f9'));
sync2 = uint16(hex2dec('af'));
mid1 = uint16(hex2dec('21'));
mid2 = uint16(hex2dec('22'));

% Array size
prealloc = uint32(500);
a = prealloc;
b = prealloc;
view = 0;

% checks if the confidential algo is available on the system
algo_available = exist('algo.m', 'file');

%% Clear vars
clear fap
clear fbp
clear fcp
clear fdp
clear fas
clear fbs
clear fcs
clear fds
clear fat
clear fbt
clear fct
clear fdt
clear rx
clear ry
clear rz
clear tr
clear tl
clear banana;

%% Set up data arrays
fap(1:prealloc) = int32(0);
fbp(1:prealloc) = int32(0);
fcp(1:prealloc) = int32(0);
fdp(1:prealloc) = int32(0);

fas(1:prealloc) = int32(0);
fbs(1:prealloc) = int32(0);
fcs(1:prealloc) = int32(0);
fds(1:prealloc) = int32(0);

fa(1:prealloc) = double(0);
fb(1:prealloc) = double(0);
fc(1:prealloc) = double(0);
fd(1:prealloc) = double(0);

fat(1:prealloc) = int32(0);
fbt(1:prealloc) = int32(0);
fct(1:prealloc) = int32(0);
fdt(1:prealloc) = int32(0);

fat_c(1:prealloc) = double(0);
fbt_c(1:prealloc) = double(0);
fct_c(1:prealloc) = double(0);
fdt_c(1:prealloc) = double(0);

rx(1:prealloc) = double(0);
ry(1:prealloc) = double(0);
rz(1:prealloc) = double(0);
tr(1:prealloc) = double(0);
tl(1:prealloc) = double(0);

% In honor of the Minions:
banana = uint16(0); %complete transmitted data

% Time counter and lap counter (for debugging)
t = 0;
lap(1:prealloc) = int32(0);

% Rocket signals
lo = uint8(0);      %Lift off
soe = uint8(0);     %Start of Experiment
sods = uint8(0);    %Stop of Data Storage
auxa = uint8(0);    %Digital IO
auxb = uint8(0);    %Digital IO

%% Draw 6 figures
gcf = figure(1);
set(gcf,'units','normalized','outerposition',[0 0 1 1])
%set(gca, 'LooseInset', get(gca,'TightInset'))

%set(gcf, 'KeyPressFcn', @(x,y)disp(get(gcf,'CurrentCharacter')))
global pressed;
pressed = 0;
set(gcf, 'KeyPressFcn', @(x,y)getpressedKeyGlobal(get(gcf,'CurrentCharacter')));

f1 = subplot(3, 2, 1); %fibres
f2 = subplot(3, 2, 2); %adxl
f3 = subplot(3, 2, 3); %fibre-temperatures
f4 = subplot(3, 2, 4); %board temp
f5 = subplot(3, 2, 5); %status signals

[X,map] = imread('legend.png','png');
f6 = subplot(3, 2, 6);
imshow(X,map); %legend

%% Disable tex interpreter for faster rendering
set(0, 'DefaultTextInterpreter', 'none');

%% While loop reason to run
on = 1;

%% Main loop
while on
    tic;
    if (s.BytesAvailable > 50)
        raw = uint16(fread(s, s.BytesAvailable));
    else
        if (s.BytesAvailable == 0)
            pause(.2);
        end
        raw = uint16(fread(s, 100));
    end
    
    if (isempty(raw)~=1)
        banana = [banana rot90(raw)];
    end
    
    for i = 3:(size(raw)-21)
        if raw(i) == mid1
            if raw(i-1) == sync2
                if raw(i-2) == sync1
                    
                    fap = circshift(fap, [0, -1]);
                    fas = circshift(fas, [0, -1]);
                    fap(a) =  raw(i+1)*256 + raw(i+2);
                    fas(a) =  raw(i+3)*256 + raw(i+4);
                    
                    fbp = circshift(fbp, [0, -1]);
                    fbs = circshift(fbs, [0, -1]);
                    fbp(a) =  raw(i+5)*256 + raw(i+6);
                    fbs(a) =  raw(i+7)*256 + raw(i+8);  
                    
                    fcp = circshift(fcp, [0, -1]);
                    fcs = circshift(fcs, [0, -1]);
                    fcp(a) =  raw(i+9)*256 + raw(i+10);
                    fcs(a) =  raw(i+11)*256 + raw(i+12); 
                    
                    fdp = circshift(fdp, [0, -1]);
                    fds = circshift(fds, [0, -1]);
                    fdp(a) =  raw(i+13)*256 + raw(i+14);
                    fds(a) =  raw(i+15)*256 + raw(i+16);
                    
                    fa = circshift(fa, [0, -1]);
                    fb = circshift(fb, [0, -1]);
                    fc = circshift(fc, [0, -1]);
                    fd = circshift(fd, [0, -1]);
                    
                    if (algo_available>0)
                        fa(a) = algo(fap(a), fas(a), fat_c(b), 0);
                        fb(a) = algo(fbp(a), fbs(a), fbt_c(b), 0);
                        fc(a) = algo(fcp(a), fcs(a), fct_c(b), 0);
                        fd(a) = algo(fdp(a), fds(a), fdt_c(b), 0);
                    else
                        fa(a) = fap(a);
                        fb(a) = fbp(a);
                        fc(a) = fcp(a);
                        fd(a) = fdp(a);   
                        %fa(a) = (double(fap(a)) - double(fas(a)));
                        %fb(a) = (double(fbp(a)) - double(fbs(a)));
                        %fc(a) = (double(fcp(a)) - double(fcs(a)));
                        %fd(a) = (double(fcp(a)) - double(fds(a)));
                    end
                    
                end
            end
        end
        if raw(i) == mid2
            if raw(i-1) == sync2
                if raw(i-2) == sync1
                    
                    fat = circshift(fat, [0, -1]);
                    fbt = circshift(fbt, [0, -1]);
                    fct = circshift(fct, [0, -1]);
                    fdt = circshift(fdt, [0, -1]);

                    fat(b) =  raw(i+1)*256 + raw(i+2);
                    fbt(b) =  raw(i+3)*256 + raw(i+4);
                    fct(b) =  raw(i+5)*256 + raw(i+6);
                    fdt(b) =  raw(i+7)*256 + raw(i+8);
                    
                    fat_c = circshift(fat_c, [0, -1]);
                    fbt_c = circshift(fbt_c, [0, -1]);
                    fct_c = circshift(fct_c, [0, -1]);
                    fdt_c = circshift(fdt_c, [0, -1]);
                    
                    fat_c(b) = ntc(fat(b));
                    fbt_c(b) = ntc(fbt(b));
                    fct_c(b) = ntc(fct(b));
                    fdt_c(b) = ntc(fdt(b));
                    
                    tr = circshift(tr, [0, -1]);
                    tl = circshift(tl, [0, -1]);
                    tr(b) =  double(typecast(uint16(bin2dec([dec2bin(raw(i+9), 8) dec2bin(raw(i+10), 8)])),'int16'))*0.0625;
                    tl(b) =  double(typecast(uint16(bin2dec([dec2bin(raw(i+11), 8) dec2bin(raw(i+12), 8)])),'int16'))*0.0625;
                    
                    rx = circshift(rx, [0, -1]);
                    ry = circshift(ry, [0, -1]);
                    rz = circshift(rz, [0, -1]);
                    
                    rx(b) =  double(typecast(uint16(bin2dec([dec2bin(raw(i+13), 8) dec2bin(raw(i+14), 8)])),'int16'))*2.9/1000;
                    ry(b) =  double(typecast(uint16(bin2dec([dec2bin(raw(i+15), 8) dec2bin(raw(i+16), 8)])),'int16'))*2.9/1000;
                    rz(b) =  double(typecast(uint16(bin2dec([dec2bin(raw(i+17), 8) dec2bin(raw(i+18), 8)])),'int16'))*2.9/1000;
                    
                    flags = raw(i+19);
                    auxb = bitand(flags, 1);
                    auxa = bitand(bitshift(flags, -1), 1);
                    sods = bitand(bitshift(flags, -2), 1);
                    lo = bitand(bitshift(flags, -3), 1);
                    soe = bitand(bitshift(flags, -4), 1);
                    
                end
            end
        end
    end
    
    
    
    xaxes = 1:a;
    plot(f1, xaxes, fa, xaxes, fb, xaxes, fc, xaxes, fd);
    %legend(f1, 'FiberA', 'FiberB', 'FiberC', 'FiberD');    %tooooo slow

    plot(f2, xaxes, rx, xaxes, ry, xaxes, rz);
    %ylim(f2, [-15 15]);                                    %optional
    %legend(f2, 'RefX', 'RefY', 'RefZ');                    %tooooo slow
    
    plot(f3, xaxes, fat_c, xaxes, fbt_c, xaxes, fct_c, xaxes, fdt_c);
    %ylim(f3, [-50 80]);                                    %optional
    %legend(f3, 'FiberA', 'FiberB', 'FiberC', 'FiberD');    %tooooo slow
    
    plot(f4, xaxes, tl, xaxes, tr);
    %ylim(f4, [-50 140]);                                   %optional
    %legend(f4, 'onboard LM74', 'external LM74');           %tooooo slow
    
    %Plot bar graph for digital signals
    updrate=(length(raw)/1000.);
    
    if (view == 0)
        bar(f5, [lo soe sods auxa auxb]);
        %ylim(f5, [0 1]);  %not needed
        title(f5, '          LO          /          SEO          /         SODS         /  Loss of Output  /  Pump temp. alarm', 'Color', 'Black');
        ylabel(f5, 'ON/OFF', 'FontSize', 7);
    elseif (view == 1)
        plot(f5, xaxes, fap, xaxes, fbp, xaxes, fcp, xaxes, fdp);
        title(f5, 'Fiber primary', 'Color', 'Black');
        ylabel(f5, '[LSB16]', 'FontSize', 7);
    elseif (view ==2)
        plot(f5, xaxes, fas, xaxes, fbs, xaxes, fcs, xaxes, fds);    
        title(f5, 'Fiber reference', 'Color', 'Black');
        ylabel(f5, '[LSB16]', 'FontSize', 7);
    end    
    
    title(f1, 'FBGs', 'Color', 'Blue');
    title(f2, 'Ref', 'Color', 'Red');
    title(f3, 'FBG Temperatures', 'Color', 'Green');
    title(f4, 'Board Temperatures', 'Color', 'Yellow');

    xlabel(f1, '[*20ms time]', 'FontSize', 7);
    xlabel(f2, '[*20ms time]', 'FontSize', 7);
    xlabel(f3, '[*20ms time]', 'FontSize', 7);
    xlabel(f4, '[*20ms time]', 'FontSize', 7);

    ylabel(f1, '[lamba]', 'FontSize', 7);
    ylabel(f2, '[g]', 'FontSize', 7);
    ylabel(f3, '[LSB16]', 'FontSize', 7);
    ylabel(f4, '[�C]', 'FontSize', 7);

    pause(.01);
    
    % Textual user interface
    if pressed == 'Q'
        on = 0;
        pressed = 0;
    end
    
    if pressed == 'X'
        on = 0;
        close(gcf);
        pressed = 0;
    end
    
    if pressed == 'P'
        pause;
        pressed = 0;
    end
    
    if pressed == 'R'
        fovstc_reset;
        pressed = 0;
    end
    
    if pressed == 'L'
        fovstc_asetoggle;
        pressed = 0;
    end
    
    if pressed == 't'
        view = view + 1;
        if view > 2
            view = 0;
        end
        pressed = 0;
    end
    

    t = t+1;
    lap(t) = toc;
end

d = clock;
filename = ['flight_' num2str(d(1)) '-' num2str(d(2)) '-' num2str(d(3)) '_' num2str(d(4)) 'h' num2str(d(5)) 'm'];
save(['results/' filename], 'banana');
disp(['Data of banana saved to results/' filename  ' !']);




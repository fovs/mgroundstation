%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   FOVS Get current pressed key and write it in global var
%   Fibre Optic Vibration Experiment, www.fovs.de
%
%   Author: Thomas Gruebler, thomas.gruebler@tum.de
%   Date: 10.12.2013
%
%  "THE BEER-WARE LICENSE" (Revision 42):
%  <thomas@gruebler.at> wrote this file. As long as you retain this notice you
%  can do whatever you want with this stuff. If we meet some day, and you think
%  this stuff is worth it, you can buy me a beer in return 
%  (preferably on the Oktoberfest) - Thomas Gr�bler
%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function getpressedKeyGlobal(data)
    global pressed;
    pressed = data;
    %disp(data);
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   FOVS NTC
%   get NTC temperature from adc value
%      
%   Author: Thomas Gruebler, thomas.gruebler@tum.de
%   Date: 10.12.2013
%
%  "THE BEER-WARE LICENSE" (Revision 42):
%  <thomas@gruebler.at> wrote this file. As long as you retain this notice you
%  can do whatever you want with this stuff. If we meet some day, and you think
%  this stuff is worth it, you can buy me a beer in return 
%  (preferably on the Oktoberfest) - Thomas Gr�bler
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [temp_c, temp_k] = ntc( adc )

    pullup = 68000;
    volt = double(adc) *2.5 / (2^16);
    i = (3.3-volt)/pullup;
    r_ntc = volt / i;

    B = 3380;
    temp_k = B*298.15 / (B + log(r_ntc/10000)*298.15);

    temp_c = temp_k-273.15;
    
    %if (temp_c < -80) %workaround for missing hardware
    %    temp_c = 20;
    %end
end

